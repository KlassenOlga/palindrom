#include "DV.h"

s_Queue* createQueue()
{	
	s_Queue* newQueue = (s_Queue*)malloc(sizeof(s_Queue));
	newQueue->beginn = NULL;
	newQueue->ende = NULL;
	return newQueue;
}

s_Element createElement()
{
	s_Element element;
	scanf_s("%d", &element.numb);
	return element;
}

s_Queue* pushFromBack(s_Queue * _queue, s_Element element)
{	if (_queue==NULL)
{
	return NULL;
}

	s_Element* pNode = (s_Element*)malloc(sizeof(s_Element));
	*pNode = element;
	pNode->next = NULL;
	if (_queue->beginn==NULL)
	{
		pNode->previous = NULL;
		_queue->beginn = pNode;
	}
	else{
		_queue->ende->next = pNode;
		pNode->previous = _queue->ende;
	}
	_queue->ende = pNode;
	return pNode;
}

s_Queue * pushFromBeginn(s_Queue * _queue, s_Element element)
{
	s_Element* pNode = (s_Element*)malloc(sizeof(s_Element));
	*pNode = element;
	pNode->previous = NULL;
	
	if (_queue->beginn==NULL)
	{
		pNode->next = NULL;
		_queue->ende = pNode;
	}

	else{
		pNode->next = _queue->beginn;
		_queue->beginn->previous = pNode;
	}

	_queue->beginn = pNode;

	return pNode;
}

s_Queue* popFromBeginn(s_Queue* _queue, s_Element* element)
{
	if (_queue==NULL || _queue->beginn==NULL)
	{
		return NULL;
	}
	s_Element* pNode = _queue->beginn->next;
	
	*element = *_queue->beginn;
	free(_queue->beginn);
	_queue->beginn = pNode;
	_queue->beginn->previous = NULL;
	return _queue;
}

s_Queue * popFromEnde(s_Queue * _queue, s_Element * element)
{
	if (_queue==NULL|| _queue->beginn==NULL)
	{
		return NULL;
	}
	*element = *_queue->ende;

	s_Element* pNode = _queue->ende->previous;
	free(_queue->ende);
	_queue->ende = pNode;
	_queue->ende->next = NULL;


	return _queue;
}



void printElements(s_Queue * _queue)
{
	if (_queue == NULL || _queue->beginn == NULL)
	{
		return NULL;
	}
	s_Element* pNode = _queue->beginn;
	
	while (pNode!=NULL)
	{
		printf("%d", pNode->numb);
		pNode = pNode->next;
	}

}

s_Queue * pushElement(s_Queue * _queue, s_Element _element, int _position)
{
	if (_queue == NULL || _queue->beginn == NULL)
	{
		return NULL;
	}
	s_Element* pNode = _queue->beginn;
	

	int z=0;

	if (_position == 0)
	{
		pushFromBeginn(_queue, _element);
	}

	while (pNode->next != NULL && z!=_position)
	{
		pNode = pNode->next;
		z++;
		
	}
	if (pNode->next!=NULL)
	{
		s_Element* nKnote = (s_Element*)malloc(sizeof(s_Element));
		*nKnote = _element;
		nKnote->previous = pNode;
		nKnote->next = pNode->next;
		pNode->next->previous = nKnote;
		pNode->next = nKnote;
	}
	else if (pNode->next==NULL && _position==z)
	{
		pushFromBack(_queue, _element);
	}

	return _queue;
}



s_Queue* popElement(s_Queue *_queue, s_Element* _element, int index){

	if (_queue==NULL)
	{
		return NULL;
	}
	
	s_Element* pNode = _queue->beginn;
	int z = 0;
	while (pNode->next!=NULL && index != z)
	{

		pNode = pNode->next;
		z++;
		
	}
	if (index==z)
	{
		*_element = *pNode;
		//s_Element* zus = pNode->previous;
		pNode->previous->next = pNode->next;
		pNode->next->previous = pNode->previous;
		pNode->previous = NULL;
		pNode->next=NULL,
		free(pNode);
	}

}











s_Queue * popElement(s_Queue * _queue, int _position, s_Element* _element)
{
	if (_queue == NULL || _queue->beginn == NULL)
	{
		return NULL;
	}
	if (_position==0)
	{
		popFromBeginn(_queue, _element);
	}
	s_Element* pNode = _queue->beginn;
	int z = 0;
	while (z!=_position && pNode->next != NULL)
	{
		pNode = pNode->next;
		z++;
	}
	if (z==_position && pNode->next != NULL)
	{
		
		pNode->next->previous = pNode->previous;
		pNode->previous->next = pNode->next;
		pNode->next = NULL;
		pNode->previous = NULL;
		free(pNode);
	}
	else if(pNode->next==_queue->ende){
		popFromEnde(_queue, _element);
	}

	return _queue;
}
