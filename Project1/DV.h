#pragma once
#include <stdio.h>
#include <stdbool.h>
#include <malloc.h>
#include <stdlib.h>
#include <string.h>

typedef struct Element{
	char* array;
	int numb;
	struct Element* next;
	struct Element*previous;

}s_Element;


typedef struct Queue{
	s_Element* beginn;
	s_Element* ende;
}s_Queue;

s_Queue* createQueue();
s_Element createElement();
s_Queue* pushFromBack(s_Queue* _queue, s_Element element);
s_Queue* pushFromBeginn(s_Queue* _queue, s_Element element);
s_Queue* popFromBeginn(s_Queue* _queue, s_Element* element);
s_Queue* popFromEnde(s_Queue* _queue, s_Element* element);

s_Queue* pushElement(s_Queue* _queue, s_Element _element, int _position);
s_Queue* popElement(s_Queue* _queue, int _position, s_Element* _element);

void printElements(s_Queue* _queue);